alert('Connected');

let firstName = 'John';
let lastName = 'Smith';
let age = 30;
console.log('First Name:' + ' ' + firstName);
console.log('Last Name:' + ' ' + lastName);
console.log('Age:' + ' ' + age);

console.log('Hobbies:');
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
console.log(hobbies);

console.log('Address:');
let address = {
	houseNumber: '32',
	city: 'Lincoln',
	street: 'Washington',
	state: 'Nebraska',
};
console.log(address);

function sentence(firstName, lastName, age){
	console.log(firstName + ' ' + lastName + ' is ' + age + ' years of age'),
	console.log('This was printed inside the function')
};

sentence('John', 'Smith', 30);

function sentence2(){
	let hobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
	console.log(hobbies);
	console.log('This was printed inside the function')
};

sentence2();

function sentence3(){
	let address = {
	houseNumber: '32',
	city: 'Lincoln',
	street: 'Washington',
	state: 'Nebraska',
	};
	console.log(address);
};

sentence3();

let isMarried = true;
function status(){
	return 'The value of isMarried is:' + ' ' + isMarried
	console.log('This message will not be printed')
}

let maritalstatus = status();
console.log(maritalstatus);

